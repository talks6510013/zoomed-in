# layered-service

Sample project that should represent the concepts of
a [Layered Architecture](https://en.wikipedia.org/wiki/Multitier_architecture) in an
opinionated way.

## Local development

Start the local infrastructure with help of `src/main/local/docker-compose.yml` and the Spring Boot application with profile `local` enabled.

### Read messages published to SNS topic article

For local development purpose with help of LocalStack an SQS queue is created and a corresponding subscription to receive messages from the topic.

You can read those messages by executing the following command:

```bash
aws --endpoint="http://localhost:4566" --region=eu-central-1 sqs receive-message --queue-url http://localhost:4566/000000000000/article
```
