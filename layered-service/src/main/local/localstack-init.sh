#!/usr/bin/env bash

awslocal sns create-topic --name article
awslocal sqs create-queue --queue-name article
awslocal sns subscribe \
  --topic-arn arn:aws:sns:eu-central-1:000000000000:article \
  --protocol sqs \
  --notification-endpoint arn:aws:sqs:eu-central-1:000000000000:article \
  --attributes RawMessageDelivery=true
