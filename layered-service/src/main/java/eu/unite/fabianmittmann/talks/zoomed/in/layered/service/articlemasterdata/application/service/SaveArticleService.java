package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;

import com.leakyabstractions.result.Result;
import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.messaging.sns.ArticlePublisher;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository.ArticleRepository;
import jakarta.validation.Validator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Validated
@Slf4j
@RequiredArgsConstructor
public class SaveArticleService {

    private final ArticleRepository articleRepository;

    private final ArticlePublisher articlePublisher;

    private final Validator validator;

    @Transactional
    public Result<ArticleDto, List<ObjectError>> saveArticle(
            @NonNull final ArticleDto articleDto) {
        return validate(articleDto)
                .mapSuccess(ArticleDto::toEntity)
                .mapSuccess(articleToSave -> {
                    final var savedArticle = articleRepository.save(articleToSave);
                    articlePublisher.publish(savedArticle);

                    log.info("saved Article with ID {}", savedArticle.id());

                    return articleToSave;
                })
                .mapSuccess(ArticleDto::fromEntity);
    }

    private Result<ArticleDto, List<ObjectError>> validate(
            @NonNull final ArticleDto articleDto) {
        return Optional.of(validator.validate(articleDto))
                .filter(violations -> !violations.isEmpty())
                .map(violations -> violations.stream()
                        .map(violation -> {
                            final var violatedProp = violation.getPropertyPath().toString();
                            final var msg = violatedProp + ": " + violation.getMessage();
                            return new ObjectError(violatedProp, msg);
                        })
                        .toList())
                .map(Results::<ArticleDto, List<ObjectError>> failure)
                .orElse(Results.success(articleDto));
    }

}
