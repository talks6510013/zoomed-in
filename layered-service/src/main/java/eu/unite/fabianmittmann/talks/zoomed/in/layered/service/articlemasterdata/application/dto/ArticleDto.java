package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.NonNull;

@Builder
public record ArticleDto(
        @Nullable @Positive BigInteger id,
        @NotBlank @Size(max = 255) String name,
        @Nullable @Size(max = 255) String description,
        @NotNull @Positive @Digits(integer = Integer.MAX_VALUE, fraction = 2) BigDecimal price) {

    public static ArticleDto fromEntity(@NonNull final Article article) {
        return Mappers.getMapper(ArticleMapper.class).entityToDto(article);
    }

    public Article toEntity() {
        return Mappers.getMapper(ArticleMapper.class).dtoToEntity(this);
    }

    @Mapper
    public interface ArticleMapper {
        Article dtoToEntity(ArticleDto articleDto);

        ArticleDto entityToDto(Article article);
    }

}
