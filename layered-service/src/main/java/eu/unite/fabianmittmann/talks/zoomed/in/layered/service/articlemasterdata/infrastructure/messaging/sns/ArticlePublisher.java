package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.messaging.sns;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;
import io.awspring.cloud.messaging.core.NotificationMessagingTemplate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;

@Component
@Slf4j
@RequiredArgsConstructor
public class ArticlePublisher {

    private final ArticleOutboxRepository articleOutboxRepository;

    private final ObjectMapper objectMapper;

    private final NotificationMessagingTemplate notificationMessagingTemplate;

    @Value("${article.publish.topic}")
    private String articlePublishTopic;

    public void publish(@NonNull final Article article) {
        final var articlePayload = toJson(article);
        final var outboxEntry = new ArticleOutboxEntry(
                UUID.randomUUID(),
                article.id(),
                articlePayload,
                LocalDate.now());

        articleOutboxRepository.save(outboxEntry);
    }

    @Scheduled(fixedDelay = 1, timeUnit = TimeUnit.SECONDS)
    @SchedulerLock(name = "article-outbox-handler")
    public void handleOutboxEntries() {
        final var pageRequest = PageRequest.ofSize(1000);
        final var entriesToPublish = articleOutboxRepository.findAllByOrderByTimestamp(pageRequest);

        entriesToPublish.forEach(articleOutboxEntry -> {
            notificationMessagingTemplate
                    .convertAndSend(articlePublishTopic, articleOutboxEntry.payload());

            log.info("published data for Article {} to SNS", articleOutboxEntry.articleId());

            articleOutboxRepository.delete(articleOutboxEntry);
        });
    }

    @SneakyThrows(JsonProcessingException.class)
    private String toJson(@NonNull final Article article) {
        return objectMapper.writeValueAsString(article);
    }

    record ArticleOutboxEntry(
            @Id @NonNull UUID id,
            @NonNull BigInteger articleId,
            @NonNull String payload,
            @NonNull LocalDate timestamp) {
    }

    interface ArticleOutboxRepository extends MongoRepository<ArticleOutboxEntry, UUID> {
        List<ArticleOutboxEntry> findAllByOrderByTimestamp(Pageable pageable);
    }
}
