package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository;

import java.math.BigInteger;

import org.springframework.data.mongodb.repository.MongoRepository;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;

public interface ArticleRepository extends MongoRepository<Article, BigInteger> {
}
