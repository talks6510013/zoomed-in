package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.presentation;

import java.math.BigInteger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.PageDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service.RemoveArticleService;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service.SaveArticleService;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service.FindArticleService;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
class ArticleManagementController {

    private static final PageDto DEFAULT_PAGE = new PageDto(1, 10);

    private final FindArticleService findArticleService;

    private final RemoveArticleService removeArticleService;

    private final SaveArticleService saveArticleService;

    @GetMapping("/")
    String index(final Model model) {
        final var articles = findArticleService
                .findArticles(DEFAULT_PAGE)
                .getSuccess()
                .orElseThrow();

        model.addAttribute("articles", articles);

        return "index";
    }

    @GetMapping("/add-article")
    String addArticle(final Model model) {
        model.addAttribute("article", ArticleDto.builder().build());

        return "article-form";
    }

    @GetMapping("/edit-article/{id}")
    String editArticle(@PathVariable final String id, final Model model) {
        final var articleToUpdate = findArticleService
                .findArticleById(new BigInteger(id))
                .orElseThrow();

        model.addAttribute("article", articleToUpdate);

        return "article-form";
    }

    @PostMapping("/save-article")
    String saveArticle(@ModelAttribute("article") final ArticleDto article,
            final BindingResult bindingResult) {
        return saveArticleService.saveArticle(article)
                .mapSuccess(savedArticle -> "redirect:/")
                .orElseMap(validationErrors -> {
                    validationErrors.forEach(bindingResult::addError);
                    return "article-form";
                });
    }

    @PostMapping("/remove-article/{id}")
    String removeArticle(@PathVariable final String id) {
        final var articleToRemove = findArticleService
                .findArticleById(new BigInteger(id))
                .orElseThrow();

        removeArticleService.removeArticle(articleToRemove);

        return "redirect:/";
    }

}
