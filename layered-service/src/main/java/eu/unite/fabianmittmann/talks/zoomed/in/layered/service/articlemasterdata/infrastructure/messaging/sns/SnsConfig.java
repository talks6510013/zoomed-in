package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.messaging.sns;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;

import io.awspring.cloud.messaging.core.NotificationMessagingTemplate;

@Configuration
class SnsConfig {

    @Value("${cloud.aws.credentials.access-key}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secret-key}")
    private String secretKey;

    @Value("${cloud.aws.sns.endpoint}")
    private String snsEndpoint;

    @Value("${cloud.aws.region.static}")
    private String region;

    @Bean
    AmazonSNS amazonSNS() {
        final var awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        final var credentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        final var endpointConfiguration = new EndpointConfiguration(snsEndpoint, region);

        return AmazonSNSClient.builder()
                .withCredentials(credentialsProvider)
                .withEndpointConfiguration(endpointConfiguration)
                .build();
    }

    @Bean
    NotificationMessagingTemplate notificationMessagingTemplate(final AmazonSNS amazonSNS) {
        return new NotificationMessagingTemplate(amazonSNS);
    }

}
