package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.client.MongoClient;

import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.mongo.MongoLockProvider;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;

@Configuration
@EnableSchedulerLock(defaultLockAtMostFor = "60m")
class ShedlockConfig {

    @Bean
    LockProvider lockProvider(final MongoClient mongo,
            @Value("${spring.data.mongodb.database}") final String databaseName) {
        return new MongoLockProvider(mongo.getDatabase(databaseName));
    }

}
