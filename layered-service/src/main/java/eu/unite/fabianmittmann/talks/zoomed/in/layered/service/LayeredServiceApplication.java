package eu.unite.fabianmittmann.talks.zoomed.in.layered.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LayeredServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LayeredServiceApplication.class, args);
    }

}
