package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.LayeredServiceApplication;

@Configuration
@EnableMongoRepositories(
        basePackageClasses = LayeredServiceApplication.class,
        considerNestedRepositories = true)
class MongoDbConfig {

    /**
     * Enables @Transactional support.
     */
    @Bean
    MongoTransactionManager transactionManager(MongoDatabaseFactory dbFactory) {
        return new MongoTransactionManager(dbFactory);
    }

}
