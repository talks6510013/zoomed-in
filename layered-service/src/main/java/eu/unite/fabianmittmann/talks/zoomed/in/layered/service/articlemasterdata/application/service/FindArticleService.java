package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.ObjectError;

import com.leakyabstractions.result.Result;
import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.PageDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository.ArticleRepository;
import jakarta.validation.Validator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindArticleService {

    private final ArticleRepository articleRepository;

    private final Validator validator;

    @Transactional(readOnly = true)
    public Result<List<ArticleDto>, List<ObjectError>> findArticles(
            @NonNull final PageDto pageDto) {
        return validate(pageDto)
                .mapSuccess(validPageDto -> {
                    final var pageRequest = PageRequest.of(
                            validPageDto.page() - 1,
                            validPageDto.size(),
                            Sort.by("id"));
                    final var articles = articleRepository.findAll(pageRequest);

                    return articles.stream()
                            .map(ArticleDto::fromEntity)
                            .toList();
                });
    }

    @Transactional(readOnly = true)
    public Optional<ArticleDto> findArticleById(@NonNull final BigInteger id) {
        return articleRepository
                .findById(id)
                .map(ArticleDto::fromEntity);
    }

    private Result<PageDto, List<ObjectError>> validate(
            @NonNull final PageDto pageDto) {
        return Optional.of(validator.validate(pageDto))
                .filter(violations -> !violations.isEmpty())
                .map(violations -> violations.stream()
                        .map(violation -> {
                            final var violatedProp = violation.getPropertyPath().toString();
                            final var msg = violatedProp + ": " + violation.getMessage();
                            return new ObjectError(violatedProp, msg);
                        })
                        .toList())
                .map(Results::<PageDto, List<ObjectError>> failure)
                .orElse(Results.success(pageDto));
    }

}
