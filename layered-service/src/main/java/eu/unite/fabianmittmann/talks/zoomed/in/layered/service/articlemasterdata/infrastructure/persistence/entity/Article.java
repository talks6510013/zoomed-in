package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.annotation.Nullable;
import lombok.NonNull;

@Document
public record Article(
        @Id BigInteger id,
        @NonNull String name,
        @Nullable String description,
        @NonNull BigDecimal price) {
}
