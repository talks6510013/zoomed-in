package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service;

import org.springframework.stereotype.Service;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository.ArticleRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class RemoveArticleService {

    private final ArticleRepository articleRepository;

    public void removeArticle(@NonNull final ArticleDto articleDto) {
        final var articleToRemove = articleDto.toEntity();

        articleRepository.delete(articleToRemove);
        log.info("removed Article with ID {}", articleToRemove.id());
    }

}
