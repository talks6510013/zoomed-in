package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
class SchedulerConfig {
    // intentionally left blank
}
