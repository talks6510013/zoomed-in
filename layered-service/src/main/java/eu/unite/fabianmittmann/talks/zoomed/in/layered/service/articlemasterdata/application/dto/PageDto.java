package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

public record PageDto(
        @NotNull @Positive Integer page,
        @NotNull @Positive Integer size) {
}
