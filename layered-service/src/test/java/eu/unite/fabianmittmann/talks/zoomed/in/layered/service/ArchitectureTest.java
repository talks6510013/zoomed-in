package eu.unite.fabianmittmann.talks.zoomed.in.layered.service;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import org.junit.jupiter.api.Test;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;

class ArchitectureTest {

    private static final String PROJECT_PACKAGE = "eu.unite.fabianmittmann.talks.zoomed.in.layered.service..";

    private static final String PRESENTATION_PACKAGE = "..presentation..";

    private static final String APPLICATION_PACKAGE = "..application..";

    private static final String INFRASTRUCTURE_PACKAGE = "..infrastructure..";

    private static final JavaClasses CLASSES = new ClassFileImporter().importPackages(PROJECT_PACKAGE);

    @Test
    void checkPresentationDependencies() {
        classes().that()
                .resideInAPackage(PRESENTATION_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAPackage(PRESENTATION_PACKAGE)
                .check(CLASSES);
    }

    @Test
    void checkApplicationDependencies() {
        classes().that()
                .resideInAPackage(APPLICATION_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage(APPLICATION_PACKAGE, PRESENTATION_PACKAGE)
                .check(CLASSES);
    }

    @Test
    void checkInfrastructureDependencies() {
        classes().that()
                .resideInAPackage(INFRASTRUCTURE_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage(INFRASTRUCTURE_PACKAGE, APPLICATION_PACKAGE, PRESENTATION_PACKAGE)
                .check(CLASSES);
    }

}
