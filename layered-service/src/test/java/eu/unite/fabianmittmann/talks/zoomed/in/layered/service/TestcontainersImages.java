package eu.unite.fabianmittmann.talks.zoomed.in.layered.service;

import org.testcontainers.utility.DockerImageName;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestcontainersImages {

    public static DockerImageName MONGO = DockerImageName.parse("mongo:6.0.5");

    public static DockerImageName LOCALSTACK = DockerImageName.parse("localstack/localstack:1.4");

}
