package eu.unite.fabianmittmann.talks.zoomed.in.layered.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.containers.localstack.LocalStackContainer.Service;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.amazonaws.regions.Regions;

@SpringBootTest
@ActiveProfiles("test")
@Testcontainers
class LayeredServiceApplicationIT {

    @Container
    static MongoDBContainer mongoDbContainer = new MongoDBContainer(TestcontainersImages.MONGO);

    @Container
    static LocalStackContainer localStackContainer = new LocalStackContainer(
            TestcontainersImages.LOCALSTACK)
                    .withServices(Service.SNS)
                    .withEnv("DEFAULT_REGION", Regions.EU_CENTRAL_1.getName());

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", mongoDbContainer::getReplicaSetUrl);
        registry.add("cloud.aws.sns.endpoint", () -> localStackContainer.getEndpointOverride(
                Service.SNS).toString());
    }

    @BeforeAll
    static void beforeAll() throws Exception {
        localStackContainer.execInContainer("awslocal", "sns", "create-topic",
                "--region eu-central-1", "--name", "article");
    }

    @Test
    void contextLoads() {
        assertTrue(true);
    }

}
