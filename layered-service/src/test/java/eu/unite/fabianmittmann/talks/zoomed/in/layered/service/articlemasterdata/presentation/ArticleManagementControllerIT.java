package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.presentation;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.ObjectError;

import com.leakyabstractions.result.Result;
import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service.FindArticleService;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service.RemoveArticleService;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service.SaveArticleService;

@WebMvcTest(controllers = ArticleManagementController.class)
@ActiveProfiles("test")
class ArticleManagementControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    FindArticleService findArticleService;

    @MockBean
    RemoveArticleService removeArticleService;

    @MockBean
    SaveArticleService saveArticleService;

    @Test
    void index_thenExpectLoadedArticlesWithinIndexModel() throws Exception {
        // arrange
        final var article1 = new ArticleDto(
                BigInteger.valueOf(123L),
                "article 1",
                "description 1",
                new BigDecimal(321));
        final var article2 = new ArticleDto(
                BigInteger.valueOf(1234L),
                "article 2",
                "description 2",
                new BigDecimal(4321));
        final Result<List<ArticleDto>, List<ObjectError>> mockedArticles = Results
                .success(List.of(article1, article2));

        when(findArticleService.findArticles(any())).thenReturn(mockedArticles);

        // act + assert
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("articles", mockedArticles.getSuccess()
                        .orElseThrow()));
    }

    @Test
    void addArticle_thenExpectEmptyArticleWithinFormModel() throws Exception {
        // act + assert
        mockMvc.perform(get("/add-article"))
                .andExpect(status().isOk())
                .andExpect(view().name("article-form"))
                .andExpect(model().attribute("article", ArticleDto.builder().build()));
    }

    @Test
    void editArticle_thenExpectLoadedArticleWithinFormModel() throws Exception {
        // arrange
        final var article = new ArticleDto(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321));
        when(findArticleService.findArticleById(any())).thenReturn(Optional.of(article));

        // act + assert
        mockMvc.perform(get("/edit-article/123"))
                .andExpect(status().isOk())
                .andExpect(view().name("article-form"))
                .andExpect(model().attribute("article", article));
    }

    @Test
    void saveArticle_whenArticleDataValid_thenExpectRedirectToIndexAndVerifySave() throws Exception {
        // arrange
        final var article = new ArticleDto(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321));
        when(saveArticleService.saveArticle(any())).thenReturn(Results.success(article));

        // act + assert
        mockMvc.perform(post("/save-article")
                .param("id", article.id().toString())
                .param("name", article.name())
                .param("description", article.description())
                .param("price", article.price().toString()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"))
                .andExpect(model().hasNoErrors());

        verify(saveArticleService).saveArticle(new ArticleDto(
                BigInteger.valueOf(123L),
                "article",
                "description",
                BigDecimal.valueOf(321L)));
        verifyNoMoreInteractions(saveArticleService);
    }

    @Test
    void saveArticle_whenArticleDataInvalid_thenExpectErrorsShownAtForm() throws Exception {
        // arrange
        final var validationErrors = List.of(
                new ObjectError("name", "name is erroneous"),
                new ObjectError("description", "invalid description"),
                new ObjectError("price", "price is not present"));

        when(saveArticleService.saveArticle(any())).thenReturn(Results.failure(validationErrors));
        
        // act + assert
        mockMvc.perform(post("/save-article")
                        .param("id", "123")
                        .param("name", "some erroneous name")
                        .param("description", "invalid description")
                        .param("price", "i am a price and not present"))
                .andExpect(status().isOk())
                .andExpect(view().name("article-form"))
                .andExpect(model().hasErrors());
    }

    @Test
    void removeArticle_thenExpectRedirectToIndexAndVerifyRemoval() throws Exception {
        // arrange
        final var article = new ArticleDto(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321));
        when(findArticleService.findArticleById(any())).thenReturn(Optional.of(article));

        // act + assert
        mockMvc.perform(post("/remove-article/123"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));

        verify(removeArticleService).removeArticle(article);
        verifyNoMoreInteractions(removeArticleService);
    }

}