package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.TestcontainersImages;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;

@DataMongoTest
@Testcontainers
class ArticleRepositoryIT {

    @Container
    static MongoDBContainer mongoDbContainer = new MongoDBContainer(TestcontainersImages.MONGO);

    @Autowired
    ArticleRepository uut;

    @Autowired
    MongoTemplate mongoTemplate;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", mongoDbContainer::getReplicaSetUrl);
    }

    @Test
    void findAll_when3ValuesInDbAndPageIs1AndSizeIs2_thenExpect1stAnd2ndValue() {
        // arrange
        final var article1 = new Article(
                BigInteger.valueOf(123L),
                "article 1",
                "description 1",
                new BigDecimal(321));
        final var article2 = new Article(
                BigInteger.valueOf(1234L),
                "article 2",
                "description 2",
                new BigDecimal(4321));
        final var article3 = new Article(
                BigInteger.valueOf(12345L),
                "article 3",
                "description 3",
                new BigDecimal(54321));
        mongoTemplate.save(article1);
        mongoTemplate.save(article2);
        mongoTemplate.save(article3);

        final var pageRequest = PageRequest.of(0, 2, Sort.by("id"));

        // act
        final var result = uut.findAll(pageRequest);

        // assert
        assertThat(result)
                .hasSize(2)
                .anySatisfy(article -> assertThat(article.id()).isEqualTo(123L))
                .anySatisfy(article -> assertThat(article.id()).isEqualTo(1234L));
    }

    @Test
    void findById_whenPresentInDb_thenExpectPresent() {
        // arrange
        final var article = new Article(
                BigInteger.valueOf(12345L),
                "article",
                "description",
                new BigDecimal(54321));
        mongoTemplate.save(article);

        // act
        final var result = uut.findById(BigInteger.valueOf(12345L));

        // assert
        assertThat(result).isPresent();
    }

    @Test
    void findById_whenNotPresentInDb_thenExpectEmpty() {
        // act
        final var result = uut.findById(BigInteger.valueOf(12345L));

        // assert
        assertThat(result).isEmpty();
    }

    @Test
    void save_whenAlreadyPresentInDb_thenExpectUpdatedValuesInDb() {
        // arrange
        final var article = new Article(
                BigInteger.valueOf(12345L),
                "article",
                "description",
                BigDecimal.valueOf(54321));
        mongoTemplate.save(article);

        final var updatedArticle = new Article(
                BigInteger.valueOf(12345L),
                "updated article",
                "updated description",
                BigDecimal.valueOf(54321.50));

        // act
        final var result = uut.save(updatedArticle);

        // assert
        assertThat(result.id()).isEqualTo(BigInteger.valueOf(12345L));
        assertThat(result.name()).isEqualTo("updated article");
        assertThat(result.description()).isEqualTo("updated description");
        assertThat(result.price()).isEqualTo(BigDecimal.valueOf(54321.50));

        final var fromDb = mongoTemplate.findById(BigInteger.valueOf(12345L), Article.class);
        assertThat(fromDb).isNotNull();
        assertThat(fromDb.id()).isEqualTo(BigInteger.valueOf(12345L));
        assertThat(fromDb.name()).isEqualTo("updated article");
        assertThat(fromDb.description()).isEqualTo("updated description");
        assertThat(fromDb.price()).isEqualTo(BigDecimal.valueOf(54321.50));
    }

    @Test
    void save_whenNotYetPresentInDb_thenExpectInsertedValuesInDb() {
        // arrange
        final var article = new Article(
                BigInteger.valueOf(12345L),
                "article",
                "description",
                BigDecimal.valueOf(54321.1));

        // act
        final var result = uut.save(article);

        // assert
        assertThat(result.id()).isEqualTo(BigInteger.valueOf(12345L));
        assertThat(result.name()).isEqualTo("article");
        assertThat(result.description()).isEqualTo("description");
        assertThat(result.price()).isEqualTo(BigDecimal.valueOf(54321.1));

        final var fromDb = mongoTemplate.findById(BigInteger.valueOf(12345L), Article.class);
        assertThat(fromDb).isNotNull();
        assertThat(fromDb.id()).isEqualTo(BigInteger.valueOf(12345L));
        assertThat(fromDb.name()).isEqualTo("article");
        assertThat(fromDb.description()).isEqualTo("description");
        assertThat(fromDb.price()).isEqualTo(BigDecimal.valueOf(54321.1));

    }

    @Test
    void remove_expectNoMorePresentInDb() {
        // arrange
        final var article = new Article(
                BigInteger.valueOf(12345L),
                "article",
                "description",
                new BigDecimal(54321));
        mongoTemplate.save(article);

        // act
        uut.delete(article);

        // assert
        assertThat(mongoTemplate.findById(BigInteger.valueOf(12345L), Article.class)).isNull();
    }

}