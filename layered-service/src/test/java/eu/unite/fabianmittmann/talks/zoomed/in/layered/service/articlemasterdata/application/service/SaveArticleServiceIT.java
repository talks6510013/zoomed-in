package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.messaging.sns.ArticlePublisher;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository.ArticleRepository;

@SpringBootTest(classes = SaveArticleService.class)
@ImportAutoConfiguration(ValidationAutoConfiguration.class)
@ActiveProfiles("test")
class SaveArticleServiceIT {

    @Autowired
    SaveArticleService uut;

    @MockBean
    ArticleRepository articleRepository;

    @MockBean
    ArticlePublisher articlePublisher;

    @ParameterizedTest
    @CsvSource({ "123,name,description,12",
            "12345678,NAME,DESCRIPTION,12.1",
            ",n a m e,,12.12" })
    void saveArticle_whenArticleDtoValid_thenExpectSuccess(
            final BigInteger id,
            final String name,
            final String description,
            final BigDecimal price) {
        // arrange
        final var articleDto = new ArticleDto(id, name, description, price);

        when(articleRepository.save(any())).thenReturn(new Article(id, name, description, price));

        // act
        final var result = uut.saveArticle(articleDto);

        // assert
        assertThat(result.hasSuccess()).isTrue();
    }

    @ParameterizedTest
    @ValueSource(longs = { -100, -1, 0 })
    void saveArticle_whenArticleIdNotValid_thenExpectFailure(final long id) {
        // arrange
        final var articleDto = new ArticleDto(BigInteger.valueOf(id), "name", "description",
                BigDecimal.valueOf(12));

        // act
        final var result = uut.saveArticle(articleDto);

        // assert
        assertThat(result.getFailure())
                .hasValueSatisfying(errorList -> assertThat(errorList).hasSize(1));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = { " " })
    @MethodSource("stringsWithMoreThan255Characters")
    void saveArticle_whenArticleNameNotValid_thenExpectFailure(final String name) {
        // arrange
        final var articleDto = new ArticleDto(BigInteger.valueOf(123L), name, "description",
                BigDecimal.valueOf(12));

        // act
        final var result = uut.saveArticle(articleDto);

        // assert
        assertThat(result.getFailure())
                .hasValueSatisfying(errorList -> assertThat(errorList).hasSize(1));
    }

    @ParameterizedTest
    @MethodSource("stringsWithMoreThan255Characters")
    void saveArticle_whenArticleDescriptionNotValid_thenExpectFailure(final String description) {
        // arrange
        final var articleDto = new ArticleDto(BigInteger.valueOf(123L), "name", description,
                BigDecimal.valueOf(12));

        // act
        final var result = uut.saveArticle(articleDto);

        // assert
        assertThat(result.getFailure())
                .hasValueSatisfying(errorList -> assertThat(errorList).hasSize(1));
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(doubles = { -100, -1, 0, 1.111, 1.2343556 })
    void saveArticle_whenArticlePriceNotValid_thenExpectFailure(final Double price) {
        // arrange
        final var priceAsBigDecimal = price == null ? null : BigDecimal.valueOf(price);
        final var articleDto = new ArticleDto(BigInteger.valueOf(123L), "name", "description",
                priceAsBigDecimal);

        // act
        final var result = uut.saveArticle(articleDto);

        // assert
        assertThat(result.getFailure())
                .hasValueSatisfying(errorList -> assertThat(errorList).hasSize(1));
    }

    private static Stream<String> stringsWithMoreThan255Characters() {
        return Stream.of(
                createStringWithLength(256),
                createStringWithLength(300));
    }

    private static String createStringWithLength(final int length) {
        return IntStream.rangeClosed(1, length)
                .mapToObj(x -> "x")
                .collect(Collectors.joining());
    }

}