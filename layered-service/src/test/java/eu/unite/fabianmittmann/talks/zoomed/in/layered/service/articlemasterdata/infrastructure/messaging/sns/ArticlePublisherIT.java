package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.messaging.sns;

import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Duration;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.containers.localstack.LocalStackContainer.Service;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.amazonaws.regions.Regions;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.TestcontainersImages;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.messaging.sns.ArticlePublisher.ArticleOutboxEntry;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.messaging.sns.ArticlePublisher.ArticleOutboxRepository;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;
import io.awspring.cloud.messaging.core.NotificationMessagingTemplate;

@SpringBootTest
@ActiveProfiles("test")
@Testcontainers
class ArticlePublisherIT {

    @Container
    static MongoDBContainer mongoDbContainer = new MongoDBContainer(TestcontainersImages.MONGO);

    @Container
    static LocalStackContainer localStackContainer = new LocalStackContainer(
            TestcontainersImages.LOCALSTACK)
                    .withServices(Service.SNS)
                    .withEnv("DEFAULT_REGION", Regions.EU_CENTRAL_1.getName());

    @Autowired
    ArticlePublisher articlePublisher;

    @SpyBean
    NotificationMessagingTemplate notificationMessagingTemplate;

    @SpyBean
    ArticleOutboxRepository articleOutboxRepository;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", mongoDbContainer::getReplicaSetUrl);
        registry.add("cloud.aws.sns.endpoint", () -> localStackContainer.getEndpointOverride(
                Service.SNS).toString());
    }

    @BeforeAll
    static void beforeAll() throws Exception {
        localStackContainer.execInContainer("awslocal", "sns", "create-topic",
                "--name", "article");
    }

    @Test
    void publish() {
        // arrange
        final var article = new Article(
                BigInteger.valueOf(12345L),
                "article",
                "description",
                BigDecimal.valueOf(54321.15));

        // act
        articlePublisher.publish(article);

        // assert
        await().atMost(Duration.ofSeconds(5))
                .untilAsserted(() -> {
                    final var expectedPayload = """
                            {
                                "id": 12345,
                                "name": "article",
                                "description": "description",
                                "price": 54321.15
                            }
                            """.replaceAll("[ \n]", "");
                    verify(notificationMessagingTemplate).convertAndSend("article",
                            expectedPayload);
                    verify(articleOutboxRepository).delete(any(ArticleOutboxEntry.class));
                });
    }

}