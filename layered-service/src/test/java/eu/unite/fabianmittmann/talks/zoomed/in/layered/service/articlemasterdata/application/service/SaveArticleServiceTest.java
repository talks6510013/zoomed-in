package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.messaging.sns.ArticlePublisher;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository.ArticleRepository;
import jakarta.validation.Validator;

@ExtendWith(MockitoExtension.class)
class SaveArticleServiceTest {

    SaveArticleService uut;

    @Mock
    ArticleRepository articleRepository;

    @Mock
    ArticlePublisher articlePublisher;

    @Mock
    Validator validator;

    @BeforeEach
    void setUp() {
        when(validator.validate(any())).thenReturn(Collections.emptySet());

        uut = new SaveArticleService(articleRepository, articlePublisher, validator);
    }

    @Test
    void saveArticle_thenExpectSavedArticleAndVerifySaveAndPublishing() {
        // arrange
        final var article = new Article(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321));
        final var articleDto = new ArticleDto(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321));

        when(articleRepository.save(any())).thenReturn(article);

        // act
        final var result = uut.saveArticle(articleDto);

        // assert
        assertThat(result.getSuccess()).hasValueSatisfying(savedArticle -> {
            assertThat(savedArticle.id()).isEqualTo(123L);
            assertThat(savedArticle.name()).isEqualTo("article");
            assertThat(savedArticle.description()).isEqualTo("description");
            assertThat(savedArticle.price()).isEqualTo(BigDecimal.valueOf(321));
        });

        verify(articleRepository).save(article);
        verifyNoMoreInteractions(articleRepository);
        verify(articlePublisher).publish(article);
        verifyNoMoreInteractions(articlePublisher);
    }

}