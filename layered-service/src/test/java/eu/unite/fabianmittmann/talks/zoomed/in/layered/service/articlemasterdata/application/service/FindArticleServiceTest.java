package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.PageDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository.ArticleRepository;
import jakarta.validation.Validator;

@ExtendWith(MockitoExtension.class)
class FindArticleServiceTest {

    FindArticleService uut;

    @Mock
    ArticleRepository articleRepository;

    @Mock
    Validator validator;

    @BeforeEach
    void setUp() {
        uut = new FindArticleService(articleRepository, validator);
    }

    @Test
    void findArticles_when3AreFoundInRepo_thenExpectListWith3Elements() {
        // arrange
        final var pageDto = new PageDto(1, 5);

        final var article1 = new Article(
                BigInteger.valueOf(123L),
                "article 1",
                "description 1",
                new BigDecimal(321));
        final var article2 = new Article(
                BigInteger.valueOf(1234L),
                "article 2",
                "description 2",
                new BigDecimal(4321));
        final var article3 = new Article(
                BigInteger.valueOf(12345L),
                "article 3",
                "description 3",
                new BigDecimal(54321));

        when(articleRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(article1, article2, article3)));
        when(validator.validate(any(), any())).thenReturn(Collections.emptySet());

        // act
        final var result = uut.findArticles(pageDto);

        // assert
        assertThat(result.getSuccess()).hasValueSatisfying(articleList -> {
            assertThat(articleList).hasSize(3);
            assertThat(articleList).anySatisfy(article -> {
                assertThat(article.id()).isEqualTo(123L);
                assertThat(article.name()).isEqualTo("article 1");
                assertThat(article.description()).isEqualTo("description 1");
                assertThat(article.price()).isEqualTo(new BigDecimal(321L));
            });
            assertThat(articleList).anySatisfy(article -> {
                assertThat(article.id()).isEqualTo(1234L);
                assertThat(article.name()).isEqualTo("article 2");
                assertThat(article.description()).isEqualTo("description 2");
                assertThat(article.price()).isEqualTo(new BigDecimal(4321L));
            });
            assertThat(articleList).anySatisfy(article -> {
                assertThat(article.id()).isEqualTo(12345L);
                assertThat(article.name()).isEqualTo("article 3");
                assertThat(article.description()).isEqualTo("description 3");
                assertThat(article.price()).isEqualTo(new BigDecimal(54321L));
            });
        });
    }

    @Test
    void findArticles_whenNoneAreFoundInRepo_thenExpectEmptyList() {
        // arrange
        final var pageDto = new PageDto(1, 5);

        when(articleRepository.findAll(any(Pageable.class))).thenReturn(Page.empty());
        when(validator.validate(any(), any())).thenReturn(Collections.emptySet());

        // act
        final var result = uut.findArticles(pageDto);

        // assert
        assertThat(result.getSuccess()).hasValue(Collections.emptyList());
    }

    @Test
    void findArticleById_whenFoundByRepo_thenExpectPresent() {
        // arrange
        final var article = new Article(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321));

        when(articleRepository.findById(any())).thenReturn(Optional.of(article));

        // act
        final var result = uut.findArticleById(BigInteger.valueOf(123L));

        // assert
        assertThat(result).contains(new ArticleDto(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321)));
    }

    @Test
    void findArticleById_whenNotFoundByRepo_thenExpectEmpty() {
        // arrange
        when(articleRepository.findById(any())).thenReturn(Optional.empty());

        // act
        final var result = uut.findArticleById(BigInteger.valueOf(123L));

        // assert
        assertThat(result).isEmpty();
    }

}