package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.PageDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository.ArticleRepository;

@SpringBootTest(classes = FindArticleService.class)
@ImportAutoConfiguration(ValidationAutoConfiguration.class)
@ActiveProfiles("test")
class FindArticleServiceIT {

    @Autowired
    FindArticleService uut;

    @MockBean
    ArticleRepository articleRepository;

    @ParameterizedTest
    @CsvSource({ "1,100", "25,1" })
    void findArticles_ifPageDtoValid_thenExpectSuccess(Integer page, Integer size) {
        // arrange
        final var pageDto = new PageDto(page, size);
        when(articleRepository.findAll(any(Pageable.class))).thenReturn(Page.empty());

        // act
        final var result = uut.findArticles(pageDto);

        // assert
        assertThat(result.hasSuccess()).isTrue();
    }

    @ParameterizedTest
    @CsvSource({ "1,", ",1", "-10,10", "-1,5", "0,1", "1,-4", "27,-1", "25000,0" })
    void findArticles_ifPageDtoNotValid_thenExpectFailure(Integer page, Integer size) {
        // arrange
        final var pageDto = new PageDto(page, size);

        // act
        final var result = uut.findArticles(pageDto);

        // assert
        assertThat(result.hasFailure()).isTrue();
        assertThat(result.getFailure())
                .hasValueSatisfying(objectErrors -> assertThat(objectErrors).hasSize(1));
    }

}