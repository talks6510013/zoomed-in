package eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.application.dto.ArticleDto;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.entity.Article;
import eu.unite.fabianmittmann.talks.zoomed.in.layered.service.articlemasterdata.infrastructure.persistence.repository.ArticleRepository;

@ExtendWith(MockitoExtension.class)
class RemoveArticleServiceTest {

    RemoveArticleService uut;

    @Mock
    ArticleRepository articleRepository;

    @BeforeEach
    void setUp() {
        uut = new RemoveArticleService(articleRepository);
    }

    @Test
    void removeArticle_thenVerifyDeletion() {
        // arrange
        final var article = new ArticleDto(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321));

        // act
        uut.removeArticle(article);

        // assert
        verify(articleRepository).delete(new Article(
                BigInteger.valueOf(123L),
                "article",
                "description",
                new BigDecimal(321)));
        verifyNoMoreInteractions(articleRepository);
    }

}