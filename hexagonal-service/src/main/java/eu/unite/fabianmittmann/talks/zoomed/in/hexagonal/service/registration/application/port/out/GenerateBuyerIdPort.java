package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;

public interface GenerateBuyerIdPort {

    /**
     * Generates an ID for a new {@link eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer}.
     *
     * @return a new buyer id
     */
    BuyerId generateId();

}
