package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.service;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leakyabstractions.result.Result;
import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.in.RegisterBuyerPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.FindBuyerByEmailPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.GenerateBuyerIdPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.PublishBuyerRegisteredPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.SaveBuyerPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.SendWelcomeNotificationPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegisteredDomainEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.InvalidRegistrationDataError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.RegisterBuyerCommand;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
class RegisterBuyerUseCase implements RegisterBuyerPort {

    private final RegisterBuyerWorkflow registerBuyerWorkflow;

    private final FindBuyerByEmailPort findBuyerByEmailPort;

    private final GenerateBuyerIdPort generateBuyerIdPort;

    private final SaveBuyerPort saveBuyerPort;

    private final SendWelcomeNotificationPort sendWelcomeNotificationPort;

    private final PublishBuyerRegisteredPort publishBuyerRegisteredPort;

    private final Validator validator;

    @Transactional
    @Override
    public Result<BuyerRegisteredDomainEvent, BuyerRegistrationError> registerBuyer(
            @NonNull final RegisterBuyerCommand command) {
        return validateIncomingData(command)
                .flatMapSuccess(validCommand -> registerBuyerWorkflow.run(
                        findBuyerByEmailPort::find,
                        generateBuyerIdPort::generateId,
                        sendWelcomeNotificationPort::send,
                        validCommand))
                .ifSuccess(buyerRegistered -> {
                    saveBuyerPort.save(buyerRegistered.registeredBuyer());
                    publishBuyerRegisteredPort.publish(buyerRegistered);

                    log.info("Registered new buyer with ID {}",
                            buyerRegistered.registeredBuyer().id().value());
                })
                .ifFailure(error -> log.info("Registering new buyer not possible: {}",
                        error.toString()));
    }

    private Result<RegisterBuyerCommand, BuyerRegistrationError> validateIncomingData(
            final RegisterBuyerCommand command) {
        return Optional.of(validator.validate(command))
                .filter(violations -> !violations.isEmpty())
                .map(violations -> violations.stream()
                        .map(ConstraintViolation::getMessage)
                        .toList())
                .map(InvalidRegistrationDataError::new)
                .map(Results::<RegisterBuyerCommand, BuyerRegistrationError>failure)
                .orElseGet(() -> Results.success(command));
    }

}
