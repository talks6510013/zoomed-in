package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.service;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.leakyabstractions.result.Result;
import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.in.CreateShoppingCartPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.out.SaveShoppingCartPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.CreateShoppingCartCommand;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.ShoppingCartCreatedDomainEvent;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
class CreateShoppingCartService implements CreateShoppingCartPort {

    private final CreateShoppingCartWorkflow createShoppingCartWorkflow;

    private final SaveShoppingCartPort saveShoppingCartPort;

    private final Validator validator;

    @Override
    public Result<ShoppingCartCreatedDomainEvent, InvalidShoppingCardDataError> createShoppingCart(
            @NonNull final CreateShoppingCartCommand command) {
        return validateCommand(command)
                .mapSuccess(createShoppingCartWorkflow::run)
                .ifSuccess(event -> {
                    saveShoppingCartPort.save(event.shoppingCart());
                    log.info("ShoppingCart created with ID {} for Buyer {}",
                            event.shoppingCart().id().value(),
                            event.shoppingCart().buyerId().value());
                })
                .ifFailure(error -> log.error("invalid data for creating shopping cart: {}",
                        error.message()));
    }

    private Result<CreateShoppingCartCommand, InvalidShoppingCardDataError> validateCommand(
            @NonNull final CreateShoppingCartCommand command) {
        return Optional.of(validator.validate(command))
                .filter(violations -> !violations.isEmpty())
                .map(violations -> {
                    final var message = violations.stream()
                            .map(ConstraintViolation::getMessage)
                            .collect(Collectors.joining(", "));
                    final var error = new InvalidShoppingCardDataError(message);
                    return Results.<CreateShoppingCartCommand, InvalidShoppingCardDataError>failure(error);
                })
                .orElseGet(() -> Results.success(command));
    }

}
