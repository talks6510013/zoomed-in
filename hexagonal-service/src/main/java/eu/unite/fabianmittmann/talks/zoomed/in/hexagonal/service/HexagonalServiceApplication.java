package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service;

import java.time.Clock;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;

@SpringBootApplication
public class HexagonalServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(HexagonalServiceApplication.class, args);
    }

    @Configuration
    @Slf4j
    static class ApplicationEventPublisherConfig {
        /**
         * makes {@link org.springframework.context.ApplicationEventPublisher}
         * asynchronous and catches errors so other event listeners are not effected by
         */
        @Bean
        ApplicationEventMulticaster applicationEventMulticaster() {
            final var applicationEventMulticaster = new SimpleApplicationEventMulticaster();
            applicationEventMulticaster.setErrorHandler(throwable -> log.error(
                    "processing Application Event failed for @EventListener", throwable));
            return applicationEventMulticaster;
        }

    }

    @Configuration
    static class ClockConfig {
        @Bean
        Clock clock() {
            return Clock.systemUTC();
        }
    }

    @Configuration
    @EnableJdbcRepositories(considerNestedRepositories = true)
    static class PersistenceConfig {
        // empty on purpose
    }

    @Configuration
    @EnableScheduling
    @EnableSchedulerLock(defaultLockAtMostFor = "60m")
    static class SchedulingConfig {
        @Bean
        LockProvider lockProvider(final DataSource dataSource) {
            return new JdbcTemplateLockProvider(dataSource);
        }
    }

}
