package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.in;

import com.leakyabstractions.result.Result;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.CreateShoppingCartCommand;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.ShoppingCartCreatedDomainEvent;
import lombok.NonNull;

public interface CreateShoppingCartPort {

    record InvalidShoppingCardDataError(@NonNull String message) {
    }

    /**
     * Creates a new shopping cart for a buyer.
     *
     * @param command
     *            contains data to create a new shopping cart
     * @return returns {@link ShoppingCartCreatedDomainEvent} on success or
     *         {@link InvalidShoppingCardDataError} on failure
     */
    Result<ShoppingCartCreatedDomainEvent, InvalidShoppingCardDataError> createShoppingCart(
            CreateShoppingCartCommand command);

}
