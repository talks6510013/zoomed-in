package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out;

import java.util.Optional;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;

public interface FindBuyerByEmailPort {

    /**
     * Searches a {@link Buyer} by {@link Email}.
     *
     * @param email an email
     * @return a buyer if found or empty
     */
    Optional<Buyer> find(Email email);

}
