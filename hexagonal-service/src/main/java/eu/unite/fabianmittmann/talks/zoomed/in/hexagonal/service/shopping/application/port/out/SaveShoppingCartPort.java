package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.out;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart;

public interface SaveShoppingCartPort {

    void save(ShoppingCart shoppingCart);

}
