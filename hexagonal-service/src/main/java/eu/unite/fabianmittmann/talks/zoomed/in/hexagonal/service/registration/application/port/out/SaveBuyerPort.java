package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;

public interface SaveBuyerPort {

    /**
     * Persistently creates or updates a {@link Buyer}.
     *
     * @param buyer
     *            the buyer to save
     */
    void save(Buyer buyer);

}
