package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;

public interface SendWelcomeNotificationPort {

    /**
     * Sends out a notification to a new {@link Buyer}.
     *
     * @param buyer a buyer to send the notification to
     */
    void send(Buyer buyer);

}
