package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.adapter.in;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out.event.BuyerRegisteredApplicationEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.in.CreateShoppingCartPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.CreateShoppingCartCommand;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
class BuyerRegisteredApplicationEventListenerAdapter {

    private final CreateShoppingCartPort createShoppingCartPort;

    @EventListener
    public void listen(@NonNull final BuyerRegisteredApplicationEvent buyerRegistered) {
        log.info("Shopping module received {}", buyerRegistered);

        final var command = new CreateShoppingCartCommand(buyerRegistered.id());
        createShoppingCartPort.createShoppingCart(command);
    }

}
