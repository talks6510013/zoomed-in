package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegisteredDomainEvent;

public interface PublishBuyerRegisteredPort {

    /**
     * Publishes a {@link BuyerRegisteredDomainEvent} event to a target that can be consumed by
     * all interested parties.
     * 
     * @param domainEvent
     *            the event to publish containing all data about a registered buyer
     */
    void publish(BuyerRegisteredDomainEvent domainEvent);

}
