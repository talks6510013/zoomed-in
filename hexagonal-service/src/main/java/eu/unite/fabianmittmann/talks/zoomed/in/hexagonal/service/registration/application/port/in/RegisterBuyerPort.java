package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.in;

import com.leakyabstractions.result.Result;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegisteredDomainEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.RegisterBuyerCommand;

public interface RegisterBuyerPort {

    /**
     * Registers a new {@link Buyer} to the system.
     *
     * @param registerBuyerCommand
     *            contains data required to register a buyer
     * @return an event containing the data of the registered buyer or an error
     */
    Result<BuyerRegisteredDomainEvent, BuyerRegistrationError> registerBuyer(
            RegisterBuyerCommand registerBuyerCommand);

}
