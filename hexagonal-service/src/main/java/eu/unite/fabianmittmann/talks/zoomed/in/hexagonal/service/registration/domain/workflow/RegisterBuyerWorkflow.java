package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.google.common.base.Preconditions;
import com.leakyabstractions.result.Result;
import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.EmailAlreadyRegisteredError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.InvalidRegistrationDataError;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.NonNull;

public class RegisterBuyerWorkflow {

    public record RegisterBuyerCommand(@NotBlank @Size(max = 255) String email) {}

    public record BuyerRegisteredDomainEvent(@NonNull Buyer registeredBuyer) {}

    public sealed interface BuyerRegistrationError permits InvalidRegistrationDataError, EmailAlreadyRegisteredError {
        record InvalidRegistrationDataError(
                @NonNull List<String> violations
        ) implements BuyerRegistrationError {
            public InvalidRegistrationDataError {
                Preconditions.checkArgument(!violations.isEmpty(), "violations cannot be empty");
                violations = Collections.unmodifiableList(violations);
            }
        }

        record EmailAlreadyRegisteredError() implements BuyerRegistrationError {}
    }

    /**
     * Performs the business process to register a new {@link Buyer} within the
     * system.
     *
     * @param findBuyerByEmail
     *            operation to load a buyer by email
     * @param generateBuyerId
     *            operation to generate an ID for a new buyer
     * @param sendWelcomeNotificationToBuyer
     *            operation to send a welcome mail to the buyer´s email address
     * @param command
     *            the buyer registration data
     *
     * @return in success case an event including information about the registered
     *         buyer or some error otherwise
     */
    public Result<BuyerRegisteredDomainEvent, BuyerRegistrationError> run(
            @NonNull final Function<Email, Optional<Buyer>> findBuyerByEmail,
            @NonNull final Supplier<BuyerId> generateBuyerId,
            @NonNull final Consumer<Buyer> sendWelcomeNotificationToBuyer,
            @NonNull final RegisterBuyerCommand command) {
        final Result<RegisterBuyerCommand, BuyerRegistrationError> validationResult =
                validateCommand(findBuyerByEmail, command);

        return validationResult.mapSuccess(validCommand -> {
            final var buyerId = generateBuyerId.get();
            final var registeredBuyer = new Buyer(
                    buyerId,
                    new Email(validCommand.email()));

            sendWelcomeNotificationToBuyer.accept(registeredBuyer);

            return new BuyerRegisteredDomainEvent(registeredBuyer);
        });
    }

    private Result<RegisterBuyerCommand, BuyerRegistrationError> validateCommand(
            final Function<Email, Optional<Buyer>> findBuyerByEmail,
            final RegisterBuyerCommand command) {
        final var violations = new ArrayList<String>();

        final var unvalidatedEmail = command.email();
        Email.checkForFormatViolation(unvalidatedEmail).ifPresent(violations::add);

        if (!violations.isEmpty()) {
            return Results.failure(new InvalidRegistrationDataError(violations));
        }

        final var validEmail = new Email(unvalidatedEmail);
        if (findBuyerByEmail.apply(validEmail).isPresent()) {
            return Results.failure(new EmailAlreadyRegisteredError());
        }

        return Results.success(command);
    }

}
