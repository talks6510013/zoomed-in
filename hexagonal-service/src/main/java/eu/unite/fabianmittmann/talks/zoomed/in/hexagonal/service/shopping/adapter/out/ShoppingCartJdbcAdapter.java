package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.adapter.out;

import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.jdbc.core.JdbcAggregateTemplate;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.out.SaveShoppingCartPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.ShoppingCartId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
class ShoppingCartJdbcAdapter implements SaveShoppingCartPort {

    private final PersistentShoppingCartSpringDataRepository persistentShoppingCartSpringDataRepository;

    private final JdbcAggregateTemplate jdbcAggregateTemplate;

    @Override
    public void save(@NonNull final ShoppingCart shoppingCart) {
        final var persistentShoppingCart = PersistentShoppingCart.fromDomain(shoppingCart);

        if (persistentShoppingCartSpringDataRepository.existsById(persistentShoppingCart.id())) {
            jdbcAggregateTemplate.update(persistentShoppingCart);
            return;
        }

        jdbcAggregateTemplate.insert(persistentShoppingCart);
    }

    @Table("shopping_cart")
    record PersistentShoppingCart(
            @Id UUID id,
            Long buyerId
    ) {
        static PersistentShoppingCart fromDomain(@NonNull final ShoppingCart shoppingCart) {
            return new PersistentShoppingCart(
                    shoppingCart.id().value(),
                    shoppingCart.buyerId().value());
        }

        ShoppingCart toDomain() {
            return new ShoppingCart(
                    new ShoppingCartId(this.id),
                    new BuyerId(this.buyerId));
        }
    }

    interface PersistentShoppingCartSpringDataRepository extends CrudRepository<PersistentShoppingCart, UUID> {
        // no custom methods
    }

}
