package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out.event;

import lombok.NonNull;

public record BuyerRegisteredApplicationEvent(@NonNull Long id, @NonNull String email) {
}
