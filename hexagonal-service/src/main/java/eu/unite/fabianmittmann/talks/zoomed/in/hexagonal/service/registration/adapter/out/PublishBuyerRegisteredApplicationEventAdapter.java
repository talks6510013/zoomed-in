package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.core.JdbcAggregateTemplate;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out.event.BuyerRegisteredApplicationEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.PublishBuyerRegisteredPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;

@Component
@RequiredArgsConstructor
class PublishBuyerRegisteredApplicationEventAdapter implements PublishBuyerRegisteredPort {

    private final BuyerRegisteredOutboxRepository buyerRegisteredOutboxRepository;

    private final JdbcAggregateTemplate jdbcAggregateTemplate;

    private final ApplicationEventPublisher eventPublisher;

    private final Clock clock;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @Override
    public void publish(@NonNull final RegisterBuyerWorkflow.BuyerRegisteredDomainEvent domainEvent) {
        final var outgoingEvent = new BuyerRegisteredApplicationEvent(
                domainEvent.registeredBuyer().id().value(),
                domainEvent.registeredBuyer().email().value());

        final var outgoingEventJson = serializeJson(outgoingEvent);
        final var outboxEntry = new BuyerRegisteredOutboxEntry(
                UUID.randomUUID(),
                outgoingEventJson,
                LocalDateTime.ofInstant(clock.instant(), clock.getZone()));

        jdbcAggregateTemplate.insert(outboxEntry);
    }

    @Scheduled(fixedDelay = 1, timeUnit = TimeUnit.SECONDS)
    @SchedulerLock(name = "buyer-registered-outbox-handler")
    void handleOutboxEntries() {
        final var page = PageRequest.ofSize(100);
        final var outboxEntriesToHandle = buyerRegisteredOutboxRepository
                .findAllByOrderByEventTimestamp(page);

        outboxEntriesToHandle.forEach(outboxEntry -> {
            final var eventToPublish = deserializeJson(outboxEntry.eventPayload());
            eventPublisher.publishEvent(eventToPublish);

            buyerRegisteredOutboxRepository.delete(outboxEntry);
        });
    }

    @SneakyThrows(JsonProcessingException.class)
    private String serializeJson(@NonNull final BuyerRegisteredApplicationEvent event) {
        return jsonMapper.writeValueAsString(event);
    }

    @SneakyThrows(JsonProcessingException.class)
    private BuyerRegisteredApplicationEvent deserializeJson(@NonNull final String eventPayload) {
        return jsonMapper.readValue(eventPayload, BuyerRegisteredApplicationEvent.class);
    }

    @Table("buyer_registered_outbox")
    record BuyerRegisteredOutboxEntry(
            @Id UUID eventId,

            String eventPayload,

            LocalDateTime eventTimestamp) {
    }

    interface BuyerRegisteredOutboxRepository extends
            CrudRepository<BuyerRegisteredOutboxEntry, UUID>,
            PagingAndSortingRepository<BuyerRegisteredOutboxEntry, UUID> {
        List<BuyerRegisteredOutboxEntry> findAllByOrderByEventTimestamp(Pageable pageable);
    }

}
