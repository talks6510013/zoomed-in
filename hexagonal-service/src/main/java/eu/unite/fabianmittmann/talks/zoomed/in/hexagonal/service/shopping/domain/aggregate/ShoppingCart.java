package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate;

import java.util.UUID;

import com.google.common.base.Preconditions;

import lombok.NonNull;

public record ShoppingCart(
        @NonNull ShoppingCartId id,
        @NonNull BuyerId buyerId) {
    public record ShoppingCartId(@NonNull UUID value) {
    }

    public record BuyerId(@NonNull Long value) {
        public BuyerId {
            Preconditions.checkArgument(value > 0,
                    "id value cannot be less than 1 but was %s", value);
        }
    }
}
