package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate;

import java.util.Optional;
import java.util.regex.Pattern;

import com.google.common.base.Preconditions;

import lombok.NonNull;

/**
 * A natural person or legal entity that can buy goods with help of the system.
 */
public record Buyer(
        @NonNull BuyerId id,
        @NonNull Email email) {

    public record BuyerId(@NonNull Long value) {
        public BuyerId {
            Preconditions.checkArgument(value > 0,
                    "id value cannot be less than 1 but was %s", value);
        }
    }

    public record Email(@NonNull String value) {
        private static final Pattern VALID_EMAIL_FORMAT = Pattern.compile("^.+@.+\\..+$");

        public Email {
            final var violationMsg = checkForFormatViolation(value);
            Preconditions.checkArgument(
                    violationMsg.isEmpty(),
                    violationMsg.orElse(null));
        }

        public static Optional<String> checkForFormatViolation(@NonNull final String email) {
            if (VALID_EMAIL_FORMAT.matcher(email).matches()) {
                return Optional.empty();
            }
            return Optional.of("'%s' is no valid email address".formatted(email));
        }
    }

}
