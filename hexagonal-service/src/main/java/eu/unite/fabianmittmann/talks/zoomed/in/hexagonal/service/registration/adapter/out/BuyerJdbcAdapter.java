package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out;

import java.util.Optional;

import org.springframework.data.annotation.Id;
import org.springframework.data.jdbc.core.JdbcAggregateTemplate;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.FindBuyerByEmailPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.GenerateBuyerIdPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.SaveBuyerPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
class BuyerJdbcAdapter implements SaveBuyerPort, GenerateBuyerIdPort, FindBuyerByEmailPort {

    private final BuyerSpringDataJdbcRepository buyerSpringDataJdbcRepository;

    private final JdbcAggregateTemplate jdbcAggregateTemplate;

    @Override
    public void save(@NonNull final Buyer buyer) {
        final var persistentBuyer = PersistentBuyer.fromDomain(buyer);

        if (buyerSpringDataJdbcRepository.existsById(persistentBuyer.id())) {
            jdbcAggregateTemplate.update(persistentBuyer);
            return;
        }

        jdbcAggregateTemplate.insert(persistentBuyer);
    }

    @Override
    public BuyerId generateId() {
        final var nextId = buyerSpringDataJdbcRepository.getNextBuyerIdFromSequence();
        return new BuyerId(nextId);
    }

    @Override
    public Optional<Buyer> find(@NonNull final Email email) {
        return buyerSpringDataJdbcRepository
                .findByEmail(email.value())
                .map(PersistentBuyer::toDomain);
    }

    @Table("buyer")
    record PersistentBuyer(
            @Id Long id,
            String email) {
        static PersistentBuyer fromDomain(@NonNull final Buyer buyer) {
            return new PersistentBuyer(
                    buyer.id().value(),
                    buyer.email().value());
        }

        Buyer toDomain() {
            return new Buyer(
                    new BuyerId(this.id),
                    new Email(this.email));
        }
    }

    interface BuyerSpringDataJdbcRepository extends CrudRepository<PersistentBuyer, Long> {
        @Query("select nextval(pg_get_serial_sequence('buyer', 'id'))")
        Long getNextBuyerIdFromSequence();

        Optional<PersistentBuyer> findByEmail(String email);
    }

}
