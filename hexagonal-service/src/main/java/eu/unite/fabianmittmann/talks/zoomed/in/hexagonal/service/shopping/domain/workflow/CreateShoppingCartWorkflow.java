package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow;

import java.util.UUID;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.ShoppingCartId;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.NonNull;

public class CreateShoppingCartWorkflow {

    public record CreateShoppingCartCommand(
            @NotNull @Positive Long buyerId) {
    }

    public record ShoppingCartCreatedDomainEvent(
            @NonNull ShoppingCart shoppingCart) {
    }

    public ShoppingCartCreatedDomainEvent run(@NonNull final CreateShoppingCartCommand command) {
        final var shoppingCart = new ShoppingCart(
                new ShoppingCartId(UUID.randomUUID()),
                new BuyerId(command.buyerId()));

        return new ShoppingCartCreatedDomainEvent(shoppingCart);
    }

}
