package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.in;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.in.RegisterBuyerPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.EmailAlreadyRegisteredError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.InvalidRegistrationDataError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.RegisterBuyerCommand;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/registration/buyer")
@RequiredArgsConstructor
class BuyerRegistrationRestAdapter {

    private final RegisterBuyerPort registerBuyerPort;

    @PostMapping
    ResponseEntity<?> register(@RequestBody final RegisterBuyerRequestPayload requestPayload) {
        final var registrationResult = registerBuyerPort.registerBuyer(
                requestPayload.toCommand());

        return registrationResult.mapSuccess(buyerRegistered -> {
            final var responsePayload = BuyerResponsePayload.fromDomain(buyerRegistered.registeredBuyer());

            return ResponseEntity.status(HttpStatus.CREATED).body(responsePayload);
        }).orElseMap(error -> {
            final var statusCode = HttpStatus.BAD_REQUEST;

            return switch (error) {
                case InvalidRegistrationDataError e ->
                        ResponseEntity.of(ProblemDetail.forStatusAndDetail(statusCode,
                                String.join(", ", e.violations()))).build();
                case EmailAlreadyRegisteredError ignored ->
                        ResponseEntity.of(ProblemDetail.forStatusAndDetail(statusCode,
                                "the given email can not be processed")).build();
            };
        });
    }

    record RegisterBuyerRequestPayload(String email) {
        RegisterBuyerCommand toCommand() {
            return new RegisterBuyerCommand(this.email);
        }
    }

    record BuyerResponsePayload(
            Long id,
            String email) {
        static BuyerResponsePayload fromDomain(@NonNull final Buyer buyer) {
            return new BuyerResponsePayload(
                    buyer.id().value(),
                    buyer.email().value());
        }
    }

}
