package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.SendWelcomeNotificationPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
class WelcomeMailAdapter implements SendWelcomeNotificationPort {

    private final WelcomeMailConfig welcomeMailConfig;

    @Override
    public void send(@NonNull final Buyer buyer) {
        log.info(
                "I should send a mail from {} to {} with subject '{}' and content '{}' but I have no implementation yet",
                welcomeMailConfig.getSender(),
                buyer.email().value(),
                welcomeMailConfig.getSubject(),
                welcomeMailConfig.getContent());
    }

    @ConfigurationProperties(prefix = "buyer.registration.mail.welcome")
    @Component
    @Validated
    @Setter
    @Getter
    static class WelcomeMailConfig {
        @Email
        @NotBlank
        private String sender;

        @NotBlank
        private String subject;

        @NotBlank
        private String content;
    }

}
