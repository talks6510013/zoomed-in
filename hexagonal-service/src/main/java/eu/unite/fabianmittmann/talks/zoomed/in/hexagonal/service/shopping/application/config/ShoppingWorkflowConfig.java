package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow;

@Configuration
class ShoppingWorkflowConfig {

    @Bean
    CreateShoppingCartWorkflow createShoppingCartWorkflow() {
        return new CreateShoppingCartWorkflow();
    }

}
