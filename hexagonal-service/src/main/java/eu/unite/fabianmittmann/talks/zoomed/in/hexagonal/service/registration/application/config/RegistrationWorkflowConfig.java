package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow;

@Configuration
class RegistrationWorkflowConfig {

    @Bean
    RegisterBuyerWorkflow registerBuyerWorkflow() {
        return new RegisterBuyerWorkflow();
    }

}
