package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.CreateShoppingCartCommand;

@ExtendWith(MockitoExtension.class)
class CreateShoppingCartWorkflowTest {

    CreateShoppingCartWorkflow uut = new CreateShoppingCartWorkflow();

    @Test
    void run_thenExpectShoppingCartCreatedEvent() {
        // arrange
        final var command = new CreateShoppingCartCommand(12345L);

        // act
        final var result = uut.run(command);

        // assert
        assertThat(result.shoppingCart().id().value()).isNotNull();
        assertThat(result.shoppingCart().buyerId().value()).isEqualTo(12345L);
    }

}