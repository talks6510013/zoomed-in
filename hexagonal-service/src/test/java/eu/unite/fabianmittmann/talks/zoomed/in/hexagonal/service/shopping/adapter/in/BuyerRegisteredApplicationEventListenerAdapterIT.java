package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.adapter.in;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out.event.BuyerRegisteredApplicationEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.in.CreateShoppingCartPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.CreateShoppingCartCommand;

@SpringBootTest(classes = BuyerRegisteredApplicationEventListenerAdapter.class)
@ActiveProfiles("test")
class BuyerRegisteredApplicationEventListenerAdapterIT {

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @MockBean
    CreateShoppingCartPort createShoppingCartPort;

    @Test
    void listen_thenExpectApplicationServiceCall() {
        // arrange
        final var event = new BuyerRegisteredApplicationEvent(1L, "my-mail@my-provider.com");

        // act
        applicationEventPublisher.publishEvent(event);

        // assert
        verify(createShoppingCartPort).createShoppingCart(new CreateShoppingCartCommand(1L));
        verifyNoMoreInteractions(createShoppingCartPort);
    }

}