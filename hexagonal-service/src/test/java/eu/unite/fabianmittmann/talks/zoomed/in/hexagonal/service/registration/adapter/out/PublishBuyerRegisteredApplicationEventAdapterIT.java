package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out.event.BuyerRegisteredApplicationEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegisteredDomainEvent;

@SpringBootTest
@ActiveProfiles("test")
class PublishBuyerRegisteredApplicationEventAdapterIT {

    @Autowired
    PublishBuyerRegisteredApplicationEventAdapter uut;

    @Autowired
    BuyerRegisteredApplicationEventTestListener buyerRegisteredApplicationEventTestListener;

    @Test
    void publish_thenExpectBuyerRegisteredEvent() {
        // arrange
        final var buyerRegistered = new BuyerRegisteredDomainEvent(new Buyer(
                new BuyerId(1L),
                new Email("my-mail@my-provider.com")));

        // act
        uut.publish(buyerRegistered);

        // assert
        await().atMost(Duration.ofSeconds(3)).untilAsserted(() -> {
            final var expectedEvent = new BuyerRegisteredApplicationEvent(1L,
                    "my-mail@my-provider.com");

            assertThat(buyerRegisteredApplicationEventTestListener.listenWithErrorInvokedEvent)
                    .isEqualTo(expectedEvent);
            assertThat(buyerRegisteredApplicationEventTestListener.listenWithSuccessInvokedEvent)
                    .isEqualTo(expectedEvent);
        });
    }

}