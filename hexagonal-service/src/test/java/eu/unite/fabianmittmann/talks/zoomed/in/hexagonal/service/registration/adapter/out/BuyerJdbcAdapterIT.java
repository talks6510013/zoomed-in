package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jdbc.core.JdbcAggregateTemplate;
import org.springframework.test.context.ActiveProfiles;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out.BuyerJdbcAdapter.PersistentBuyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;

@DataJdbcTest(includeFilters = {
        @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BuyerJdbcAdapter.class) })
@ActiveProfiles("test")
class BuyerJdbcAdapterIT {

    @Autowired
    BuyerJdbcAdapter uut;

    @Autowired
    JdbcAggregateTemplate jdbcAggregateTemplate;

    @Test
    void save_whenIdNotExistsYet_thenExpectNewBuyerPresentInDb() {
        // arrange
        final var buyerToSave = new Buyer(
                new BuyerId(123L),
                new Email("my-mail@my-provider.de"));

        // act
        uut.save(buyerToSave);

        // assert
        final var expectedId = 123L;
        final var expectedEmail = "my-mail@my-provider.de";

        final var fromDb = jdbcAggregateTemplate.findById(expectedId, PersistentBuyer.class);
        assertThat(fromDb).isNotNull();
        assertThat(fromDb.id()).isEqualTo(expectedId);
        assertThat(fromDb.email()).isEqualTo(expectedEmail);
    }

    @Test
    void save_whenIdNotExistsYet_thenExpectUpdatedBuyerPresentInDb() {
        // arrange
        final var buyer = new PersistentBuyer(456L, "my-mail@my-provider.com");
        jdbcAggregateTemplate.insert(buyer);

        final var updatedBuyer = new Buyer(
                new BuyerId(456L),
                new Email("my-other-mail@my-provider.com"));

        // act
        uut.save(updatedBuyer);

        // assert
        final var expectedId = 456;
        final var expectedEmail = "my-other-mail@my-provider.com";

        final var fromDb = jdbcAggregateTemplate.findById(expectedId, PersistentBuyer.class);
        assertThat(fromDb).isNotNull();
        assertThat(fromDb.id()).isEqualTo(expectedId);
        assertThat(fromDb.email()).isEqualTo(expectedEmail);
    }

    @Test
    void generateNewId() {
        // act
        final var result1 = uut.generateId();
        final var result2 = uut.generateId();
        final var result3 = uut.generateId();

        // assert
        assertThat(result1.value()).isEqualTo(1L);
        assertThat(result2.value()).isEqualTo(2L);
        assertThat(result3.value()).isEqualTo(3L);
    }

    @Test
    void findByEmail_whenPresent_expectFound() {
        // arrange
        final var buyer = new PersistentBuyer(456L, "my-mail@my-provider.com");
        jdbcAggregateTemplate.insert(buyer);

        // act
        final var result = uut.find(new Email("my-mail@my-provider.com"));

        // assert
        assertThat(result).isPresent();
        assertThat(result.get().id().value()).isEqualTo(456L);
        assertThat(result.get().email().value()).isEqualTo("my-mail@my-provider.com");
    }

    @Test
    void findByEmail_whenNotPresent_expectNotFound() {
        // act
        final var result = uut.find(new Email("my-mail@my-provider.com"));

        // assert
        assertThat(result).isEmpty();
    }

}
