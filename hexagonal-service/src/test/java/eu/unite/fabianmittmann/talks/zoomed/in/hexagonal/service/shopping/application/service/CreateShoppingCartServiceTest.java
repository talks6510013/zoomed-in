package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.out.SaveShoppingCartPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.ShoppingCartId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.CreateShoppingCartCommand;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.ShoppingCartCreatedDomainEvent;
import jakarta.validation.Validator;

@ExtendWith(MockitoExtension.class)
class CreateShoppingCartServiceTest {

    CreateShoppingCartService uut;

    @Mock
    CreateShoppingCartWorkflow createShoppingCartWorkflow;

    @Mock
    SaveShoppingCartPort saveShoppingCartPort;

    @Mock
    Validator validator;

    @BeforeEach
    void setUp() {
        when(validator.validate(any(CreateShoppingCartCommand.class)))
                .thenReturn(Collections.emptySet());

        uut = new CreateShoppingCartService(
                createShoppingCartWorkflow,
                saveShoppingCartPort,
                validator);
    }

    @Test
    void createShoppingCart_whenCommandIsValid_thenExpectSuccessEventAndShoppingCartSave() {
        // arrange
        final var command = new CreateShoppingCartCommand(100L);
        final var shoppingCartId = new ShoppingCartId(UUID.randomUUID());
        final var domainEvent = new ShoppingCartCreatedDomainEvent(new ShoppingCart(
                shoppingCartId, new BuyerId(100L)));

        when(createShoppingCartWorkflow.run(command)).thenReturn(domainEvent);

        // act
        final var result = uut.createShoppingCart(command);

        // assert
        assertThat(result.getSuccess()).contains(domainEvent);

        verify(saveShoppingCartPort).save(new ShoppingCart(shoppingCartId, new BuyerId(100L)));
        verifyNoMoreInteractions(saveShoppingCartPort);
    }

}