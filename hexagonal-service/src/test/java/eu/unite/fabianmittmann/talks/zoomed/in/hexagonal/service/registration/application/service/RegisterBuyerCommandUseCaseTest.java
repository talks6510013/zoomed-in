package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.FindBuyerByEmailPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.GenerateBuyerIdPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.PublishBuyerRegisteredPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.SaveBuyerPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.SendWelcomeNotificationPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegisteredDomainEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.EmailAlreadyRegisteredError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.RegisterBuyerCommand;
import jakarta.validation.Validator;

@ExtendWith(MockitoExtension.class)
class RegisterBuyerCommandUseCaseTest {

    RegisterBuyerUseCase uut;

    @Mock
    RegisterBuyerWorkflow registerBuyerWorkflow;

    @Mock
    FindBuyerByEmailPort findBuyerByEmailPort;

    @Mock
    GenerateBuyerIdPort generateBuyerIdPort;

    @Mock
    SendWelcomeNotificationPort sendWelcomeNotificationPort;

    @Mock
    SaveBuyerPort saveBuyerPort;

    @Mock
    PublishBuyerRegisteredPort publishBuyerRegisteredPort;

    @Mock
    Validator validator;

    @BeforeEach
    void setUp() {
        when(validator.validate(any())).thenReturn(Collections.emptySet());

        uut = new RegisterBuyerUseCase(
                registerBuyerWorkflow,
                findBuyerByEmailPort,
                generateBuyerIdPort,
                saveBuyerPort,
                sendWelcomeNotificationPort,
                publishBuyerRegisteredPort,
                validator);
    }

    @Test
    @SuppressWarnings("unchecked")
    void registerBuyer_verifyAllArgumentsPassedCorrectlyToWorkflow() {
        // arrange
        final var command = new RegisterBuyerCommand("my-mail@my-provider.de");
        final var registeredBuyer = new Buyer(new BuyerId(123L), new Email(
                "my-mail@my-provider.de"));
        when(registerBuyerWorkflow.run(any(), any(), any(), any()))
                .thenReturn(Results.success(new BuyerRegisteredDomainEvent(registeredBuyer)));

        // act
        uut.registerBuyer(command);

        // assert
        final var findBuyerByEmailCaptor = ArgumentCaptor.forClass(Function.class);
        final var generateBuyerIdCaptor = ArgumentCaptor.forClass(Supplier.class);
        final var sendWelcomeMailCaptor = ArgumentCaptor.forClass(Consumer.class);
        verify(registerBuyerWorkflow).run(
                findBuyerByEmailCaptor.capture(),
                generateBuyerIdCaptor.capture(),
                sendWelcomeMailCaptor.capture(),
                eq(new RegisterBuyerCommand("my-mail@my-provider.de")));
        verifyNoMoreInteractions(registerBuyerWorkflow);

        final Function<Email, Buyer> capturedFindBuyerByEmail = findBuyerByEmailCaptor.getValue();
        capturedFindBuyerByEmail.apply(null);
        verify(findBuyerByEmailPort).find(any());
        verifyNoMoreInteractions(findBuyerByEmailPort);

        final Supplier<BuyerId> capturedGenerateNewBuyerIdPort = generateBuyerIdCaptor.getValue();
        capturedGenerateNewBuyerIdPort.get();
        verify(generateBuyerIdPort).generateId();
        verifyNoMoreInteractions(generateBuyerIdPort);

        final Consumer<Buyer> capturedSendWelcomeMailPort = sendWelcomeMailCaptor.getValue();
        capturedSendWelcomeMailPort.accept(registeredBuyer);
        verify(sendWelcomeNotificationPort).send(any());
        verifyNoMoreInteractions(sendWelcomeNotificationPort);
    }

    @Test
    void registerBuyer_whenRegistrationSuccessful_thenExpectRegistrationResultAndThatBuyerIsSaved() {
        // arrange
        final var command = new RegisterBuyerCommand("my-mail@my-provider.de");
        final var registeredBuyer = new Buyer(new BuyerId(123L), new Email(
                "my-mail@my-provider.de"));
        when(registerBuyerWorkflow.run(any(), any(), any(), any()))
                .thenReturn(Results.success(new BuyerRegisteredDomainEvent(registeredBuyer)));

        // act
        final var result = uut.registerBuyer(command);

        // assert
        assertThat(result.getSuccess()).isPresent();
        assertThat(result.getSuccess().get().registeredBuyer().id().value())
                .isEqualTo(123L);
        assertThat(result.getSuccess().get().registeredBuyer().email().value())
                .isEqualTo("my-mail@my-provider.de");

        verify(saveBuyerPort).save(registeredBuyer);
        verifyNoMoreInteractions(saveBuyerPort);
        verify(publishBuyerRegisteredPort).publish(new BuyerRegisteredDomainEvent(registeredBuyer));
        verifyNoMoreInteractions(publishBuyerRegisteredPort);
    }

    @Test
    void registerBuyer_whenRegistrationFailed_thenExpectErrorResultAndNoSave() {
        // arrange
        final var command = new RegisterBuyerCommand("my-mail@my-provider.de");
        when(registerBuyerWorkflow.run(any(), any(), any(), any()))
                .thenReturn(Results.failure(new EmailAlreadyRegisteredError()));

        // act
        final var result = uut.registerBuyer(command);

        // assert
        assertThat(result.getFailure()).isPresent();
        assertThat(result.getFailure().get()).isInstanceOf(EmailAlreadyRegisteredError.class);

        verifyNoInteractions(saveBuyerPort);
        verifyNoInteractions(publishBuyerRegisteredPort);
    }

}