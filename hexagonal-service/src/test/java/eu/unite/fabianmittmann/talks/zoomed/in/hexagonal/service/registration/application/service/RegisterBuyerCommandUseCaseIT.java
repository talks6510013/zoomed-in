package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.FindBuyerByEmailPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.GenerateBuyerIdPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.PublishBuyerRegisteredPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.SaveBuyerPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.out.SendWelcomeNotificationPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegisteredDomainEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.InvalidRegistrationDataError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.RegisterBuyerCommand;

@SpringBootTest(classes = RegisterBuyerUseCase.class)
@ImportAutoConfiguration(ValidationAutoConfiguration.class)
@ActiveProfiles("test")
class RegisterBuyerCommandUseCaseIT {

    @Autowired
    RegisterBuyerUseCase uut;

    @MockBean
    RegisterBuyerWorkflow registerBuyerWorkflow;

    @MockBean
    FindBuyerByEmailPort findBuyerByEmailPort;

    @MockBean
    GenerateBuyerIdPort generateBuyerIdPort;

    @MockBean
    SendWelcomeNotificationPort sendWelcomeNotificationPort;

    @MockBean
    SaveBuyerPort saveBuyerPort;

    @MockBean
    PublishBuyerRegisteredPort publishBuyerRegisteredPort;

    @BeforeEach
    void setUp() {
        final var registeredBuyer = new Buyer(new BuyerId(1L), new Email("x@y.de"));
        when(findBuyerByEmailPort.find(any())).thenReturn(Optional.empty());
        when(registerBuyerWorkflow.run(any(), any(), any(), any()))
                .thenReturn(Results.success(new BuyerRegisteredDomainEvent(registeredBuyer)));
    }

    @Test
    void registerBuyer_whenCommandDoesNotViolateConstraints_thenExpectSuccess() {
        final var command = new RegisterBuyerCommand("abc@def.com");

        final var result = uut.registerBuyer(command);

        assertThat(result.hasSuccess()).isTrue();
        assertThat(result.getSuccess()).isPresent();
    }

    @ParameterizedTest
    @ValueSource(strings = { "   " })
    @NullAndEmptySource
    @MethodSource("stringsWithMoreThan255Characters")
    void registerBuyer_whenCommandViolatesConstraints_thenExpectFailure(final String value) {
        final var command = new RegisterBuyerCommand(value);

        final var result = uut.registerBuyer(command);

        assertThat(result.hasFailure()).isTrue();
        assertThat(result.getFailure()).isPresent();

        final var buyerRegistrationError = result.getFailure().get();
        assertThat(buyerRegistrationError).isInstanceOf(InvalidRegistrationDataError.class);
        assertThat(((InvalidRegistrationDataError) buyerRegistrationError).violations())
                .isNotEmpty();
    }

    private static Stream<String> stringsWithMoreThan255Characters() {
        return Stream.of(
                createStringWithLength(256),
                createStringWithLength(300));
    }

    private static String createStringWithLength(final int length) {
        return IntStream.rangeClosed(1, length)
                .mapToObj(i -> "x")
                .collect(Collectors.joining());
    }

}
