package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out;

import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.out.event.BuyerRegisteredApplicationEvent;
import lombok.extern.slf4j.Slf4j;

@Component
@Profile("test")
@Slf4j
class BuyerRegisteredApplicationEventTestListener {

    public BuyerRegisteredApplicationEvent listenWithErrorInvokedEvent = null;

    public BuyerRegisteredApplicationEvent listenWithSuccessInvokedEvent = null;

    @EventListener
    @Order(1)
    public void listenWithError(BuyerRegisteredApplicationEvent event) {
        listenWithErrorInvokedEvent = event;
        throw new RuntimeException("kaboom");
    }

    @EventListener
    @Order(2)
    public void listenWithSuccess(BuyerRegisteredApplicationEvent event) {
        listenWithSuccessInvokedEvent = event;
        log.info("Received test event: {}", event);
    }
}
