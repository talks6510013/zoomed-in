package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.EmailAlreadyRegisteredError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.InvalidRegistrationDataError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.RegisterBuyerCommand;

@ExtendWith(MockitoExtension.class)
class RegisterBuyerCommandWorkflowTest {

    @Mock
    Function<Email, Optional<Buyer>> findBuyerByEmail;

    @Mock
    Supplier<BuyerId> generateBuyerId;

    @Mock
    Consumer<Buyer> sendWelcomeMailToBuyer;

    final RegisterBuyerWorkflow uut = new RegisterBuyerWorkflow();

    @Test
    void run_whenRegistrationDataValidAndEmailNotRegisteredYet_expectBuyerRegisteredEventAndWelcomeMailSent() {
        // arrange
        final var email = new Email("my-mail@my-provider.de");
        final var command = new RegisterBuyerCommand(email.value());
        when(findBuyerByEmail.apply(any(Email.class))).thenReturn(Optional.empty());
        when(generateBuyerId.get()).thenReturn(new BuyerId(123L));

        // act
        final var result = uut.run(
                findBuyerByEmail,
                generateBuyerId,
                sendWelcomeMailToBuyer,
                command);

        // assert
        assertThat(result.getSuccess()).isPresent();
        final var registeredBuyer = result.getSuccess().get().registeredBuyer();
        assertThat(registeredBuyer.id().value()).isEqualTo(123L);
        assertThat(registeredBuyer.email().value()).isEqualTo("my-mail@my-provider.de");

        verify(findBuyerByEmail).apply(new Email("my-mail@my-provider.de"));
        verifyNoMoreInteractions(findBuyerByEmail);
        verify(generateBuyerId).get();
        verifyNoMoreInteractions(generateBuyerId);
        verify(sendWelcomeMailToBuyer).accept(new Buyer(
                new BuyerId(123L),
                new Email("my-mail@my-provider.de")));
        verifyNoMoreInteractions(sendWelcomeMailToBuyer);
    }

    @Test
    void run_whenRegistrationDataNotValid_thenExpectInvalidRegistrationDataErrorAndWelcomeMailNotSent() {
        // arrange
        final var command = new RegisterBuyerCommand("no-valid-email");

        // act
        final var result = uut.run(
                findBuyerByEmail,
                generateBuyerId,
                sendWelcomeMailToBuyer,
                command);

        // assert
        assertThat(result.getFailure()).isPresent();
        final var error = result.getFailure().get();
        assertThat(error).isInstanceOf(InvalidRegistrationDataError.class);
        assertThat(((InvalidRegistrationDataError) error).violations()).containsExactly(
                "'no-valid-email' is no valid email address");

        verifyNoInteractions(sendWelcomeMailToBuyer);
    }

    @Test
    void run_whenEmailIsAlreadyRegistered_thenExpectEmailAlreadyRegisteredError() {
        // arrange
        final var email = new Email("my-mail@my-provider.de");
        final var command = new RegisterBuyerCommand(email.value());
        when(findBuyerByEmail.apply(any(Email.class))).thenReturn(Optional.of(new Buyer(new BuyerId(
                123L), email)));

        // act
        final var result = uut.run(
                findBuyerByEmail,
                generateBuyerId,
                sendWelcomeMailToBuyer,
                command);

        // assert
        assertThat(result.getFailure()).isPresent();
        final var error = result.getFailure().get();
        assertThat(error).isInstanceOf(EmailAlreadyRegisteredError.class);

        verifyNoInteractions(sendWelcomeMailToBuyer);
    }

    @Test
    void invalidRegistrationDataError_whenConstructWithEmptyViolations_thenThrow() {
        // act
        final ThrowingCallable result = () -> new InvalidRegistrationDataError(
                Collections.emptyList());

        // assert
        assertThatThrownBy(result).isInstanceOf(IllegalArgumentException.class);
    }

}
