package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.UUID;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.ShoppingCartId;

class ShoppingCartTest {

    @Test
    void constructor_whenAllArgumentsNonNullAndValid_thenExpectSuccessfulConstruction() {
        final Executable result = () -> new ShoppingCart(
                new ShoppingCartId(UUID.randomUUID()),
                new BuyerId(2L));

        assertDoesNotThrow(result);
    }

    @Test
    @SuppressWarnings("DataFlowIssue")
    void constructor_whenShoppingCartIdNull_thenExpectError() {
        final ThrowingCallable result = () -> new ShoppingCart(
                null,
                new BuyerId(1L));

        assertThatThrownBy(result).isInstanceOf(NullPointerException.class);
    }

    @Test
    @SuppressWarnings("DataFlowIssue")
    void constructor_whenBuyerIdNull_thenExpectError() {
        final ThrowingCallable result = () -> new ShoppingCart(
                new ShoppingCartId(UUID.randomUUID()),
                null);

        assertThatThrownBy(result).isInstanceOf(NullPointerException.class);
    }

    @ParameterizedTest
    @ValueSource(longs = {-123L, -1L, 0})
    void constructor_whenBuyerIdValueNotPositive_thenExpectError(final Long value) {
        final ThrowingCallable result = () -> new BuyerId(value);

        assertThatThrownBy(result)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("id value cannot be less than 1 but was %s".formatted(value));
    }

}