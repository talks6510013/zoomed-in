package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.adapter.in;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.leakyabstractions.result.Results;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.application.port.in.RegisterBuyerPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegisteredDomainEvent;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.EmailAlreadyRegisteredError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.BuyerRegistrationError.InvalidRegistrationDataError;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.workflow.RegisterBuyerWorkflow.RegisterBuyerCommand;

@WebMvcTest
@ContextConfiguration(classes = BuyerRegistrationRestAdapter.class)
class BuyerRegistrationRestAdapterIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    RegisterBuyerPort registerBuyerPort;

    @Test
    void register_whenBuyerRegisteredSuccess_expectCreatedWithBuyerPayload() throws Exception {
        // arrange
        final var payload = """
                {
                  "email": "my-mail@my-provider.com"
                }
                """;
        final var registeredBuyer = new Buyer(new BuyerId(1234L), new Email("my-mail@my-provider.com"));
        when(registerBuyerPort.registerBuyer(any()))
                .thenReturn(Results.success(new BuyerRegisteredDomainEvent(registeredBuyer)));

        // act + assert
        mockMvc.perform(post("/api/registration/buyer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.id").value(1234))
                .andExpect(jsonPath("$.email").value("my-mail@my-provider.com"));
        verify(registerBuyerPort).registerBuyer(new RegisterBuyerCommand("my-mail@my-provider.com"));
    }

    @Test
    void register_whenFailedWithInvalidRegistrationData_expectBadRequestWithViolationsDetail()
            throws Exception {
        // arrange
        final var payload = """
                {
                  "email": "my-mail@my-provider.com"
                }
                """;
        final var violations = List.of("some violation", "some other violation");
        when(registerBuyerPort.registerBuyer(any()))
                .thenReturn(Results.failure(new InvalidRegistrationDataError(violations)));
        
        // act + assert
        mockMvc.perform(post("/api/registration/buyer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(payload))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.detail").value("some violation, some other violation"));
    }

    @Test
    void register_whenFailedWithEmailAlreadyRegistered_expectBadRequestWithDetailMsg()
            throws Exception {
        // arrange
        final var payload = """
                {
                  "email": "my-mail@my-provider.com"
                }
                """;
        when(registerBuyerPort.registerBuyer(any()))
                .thenReturn(Results.failure(new EmailAlreadyRegisteredError()));

        // act + assert
        mockMvc.perform(post("/api/registration/buyer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(payload))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.detail").value("the given email can not be processed"));
    }

}
