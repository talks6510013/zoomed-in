package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service;

import static com.tngtech.archunit.base.DescribedPredicate.not;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import org.junit.jupiter.api.Test;

import com.tngtech.archunit.core.domain.JavaClass.Predicates;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;

class ArchitectureTest {

    private static final String PROJECT_PACKAGE = "eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service..";

    private static final String INCOMING_ADAPTER_PACKAGE = "..adapter.in..";

    private static final String OUTGOING_ADAPTER_PACKAGE = "..adapter.out..";

    private static final String INCOMING_PORT_PACKAGE = "..application.port.in..";

    private static final String OUTGOING_PORT_PACKAGE = "..application.port.out..";

    private static final String APPLICATION_SERVICE_PACKAGE = "..application.service..";

    private static final String DOMAIN_WORKFLOW_PACKAGE = "..domain.workflow..";

    private static final String APPLICATION_CONFIG_PACKAGE = "..application.config..";

    private static final String OUTGOING_ADAPTER_EVENT_PACKAGE = "..adapter.out.event..";

    private static final JavaClasses CLASSES = new ClassFileImporter().importPackages(PROJECT_PACKAGE);

    @Test
    void classesInIncomingAdapterPackageShouldBePackagePrivate() {
        classes().that()
                .resideInAPackage(INCOMING_ADAPTER_PACKAGE)
                .should()
                .bePackagePrivate();
    }

    @Test
    void checkIncomingAdapterDependencies() {
        classes().that()
                .resideInAPackage(INCOMING_ADAPTER_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAPackage(INCOMING_ADAPTER_PACKAGE)
                .check(CLASSES);
    }

    @Test
    void classesInOutgoingAdapterPackageShouldBePackagePrivate() {
        classes().that()
                .resideInAPackage(OUTGOING_ADAPTER_PACKAGE)
                .and(not(Predicates.resideInAnyPackage(OUTGOING_ADAPTER_EVENT_PACKAGE)))
                .should()
                .bePackagePrivate()
                .check(CLASSES);
    }

    @Test
    void checkOutgoingAdapterDependencies() {
        classes().that()
                .resideInAPackage(OUTGOING_ADAPTER_PACKAGE)
                .and(not(Predicates.resideInAnyPackage(OUTGOING_ADAPTER_EVENT_PACKAGE)))
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage(OUTGOING_ADAPTER_PACKAGE)
                .check(CLASSES);
    }

    @Test
    void checkOutgoingEventDependencies() {
        classes().that()
                .resideInAPackage(OUTGOING_ADAPTER_EVENT_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage(OUTGOING_ADAPTER_PACKAGE, INCOMING_ADAPTER_PACKAGE)
                .check(CLASSES);
    }

    @Test
    void checkIncomingPortDependencies() {
        classes().that()
                .resideInAPackage(INCOMING_PORT_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage(
                        INCOMING_PORT_PACKAGE,
                        APPLICATION_SERVICE_PACKAGE,
                        INCOMING_ADAPTER_PACKAGE)
                .check(CLASSES);
    }

    @Test
    void checkOutgoingPortDependencies() {
        classes().that()
                .resideInAPackage(OUTGOING_PORT_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage(
                        OUTGOING_PORT_PACKAGE,
                        OUTGOING_ADAPTER_PACKAGE,
                        APPLICATION_SERVICE_PACKAGE)
                .check(CLASSES);
    }

    @Test
    void classesInApplicationServicePackageShouldBePackagePrivate() {
        classes().that()
                .resideInAPackage(APPLICATION_SERVICE_PACKAGE)
                .should()
                .bePackagePrivate();
    }

    @Test
    void checkApplicationServiceDependencies() {
        classes().that()
                .resideInAPackage(APPLICATION_SERVICE_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage(APPLICATION_SERVICE_PACKAGE)
                .check(CLASSES);
    }

    @Test
    void checkDomainWorkflowDependencies() {
        classes().that()
                .areNotRecords()
                .and().areNotInterfaces()
                .and().areNotEnums()
                .and().resideInAPackage(DOMAIN_WORKFLOW_PACKAGE)
                .should()
                .onlyHaveDependentClassesThat()
                .resideInAnyPackage(
                        DOMAIN_WORKFLOW_PACKAGE,
                        APPLICATION_SERVICE_PACKAGE,
                        APPLICATION_CONFIG_PACKAGE)
                .check(CLASSES);
    }

}
