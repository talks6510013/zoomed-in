package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.adapter.out;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jdbc.core.JdbcAggregateTemplate;
import org.springframework.test.context.ActiveProfiles;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.adapter.out.ShoppingCartJdbcAdapter.PersistentShoppingCart;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.ShoppingCartId;

@DataJdbcTest(includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = ShoppingCartJdbcAdapter.class))
@ActiveProfiles("test")
class ShoppingCartJdbcAdapterIT {

    @Autowired
    ShoppingCartJdbcAdapter uut;

    @Autowired
    JdbcAggregateTemplate jdbcAggregateTemplate;

    @Test
    void save_whenIdAlreadyPresentInDb_thenUpdate() {
        // arrange
        final var id = UUID.randomUUID();
        final var persistentShoppingCart = new PersistentShoppingCart(
                id, 123L);
        jdbcAggregateTemplate.insert(persistentShoppingCart);

        final var shoppingCartToSave = new ShoppingCart(
                new ShoppingCartId(id), new BuyerId(321L));

        // act
        uut.save(shoppingCartToSave);

        // assert
        final var fromDb = jdbcAggregateTemplate.findById(id, PersistentShoppingCart.class);
        assertThat(fromDb).isNotNull();
        assertThat(fromDb.id()).isEqualTo(id);
        assertThat(fromDb.buyerId()).isEqualTo(321L);
    }

    @Test
    void save_whenIdNotPresentInDbYet_thenInsert() {
        // arrange
        final var id = UUID.randomUUID();
        final var shoppingCartToSave = new ShoppingCart(
                new ShoppingCartId(id), new BuyerId(123L));

        // act
        uut.save(shoppingCartToSave);

        // assert
        final var fromDb = jdbcAggregateTemplate.findById(id, PersistentShoppingCart.class);
        assertThat(fromDb).isNotNull();
        assertThat(fromDb.id()).isEqualTo(id);
        assertThat(fromDb.buyerId()).isEqualTo(123L);
    }

}