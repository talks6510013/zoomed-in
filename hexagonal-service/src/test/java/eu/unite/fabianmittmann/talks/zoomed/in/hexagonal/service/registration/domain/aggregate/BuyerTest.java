package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.registration.domain.aggregate.Buyer.Email;

class BuyerTest {

    @Test
    void whenBuyersFieldsValid_thenExpectNoError() {
        final Executable result = () -> new Buyer(
                new BuyerId(1L),
                new Email("my-mail@my-provider.de"));

        assertDoesNotThrow(result);
    }

    @Test
    @SuppressWarnings("DataFlowIssue")
    void whenBuyersIdNull_thenExpectError() {
        final ThrowingCallable result = () -> new Buyer(null, new Email("my-mail@my-provider.de"));

        assertThatThrownBy(result).isInstanceOf(NullPointerException.class);
    }

    @Test
    @SuppressWarnings("DataFlowIssue")
    void whenBuyersEmailNull_thenExpectError() {
        final ThrowingCallable result = () -> new Buyer(new BuyerId(1L), null);

        assertThatThrownBy(result).isInstanceOf(NullPointerException.class);
    }

    @ParameterizedTest
    @ValueSource(longs = { 1, 5, 20, 1000 })
    void whenBuyerNumberNotNullAndPositive_thenExpectNoError(final Long value) {
        final Executable result = () -> new BuyerId(value);

        assertDoesNotThrow(result);
    }

    @Test
    @SuppressWarnings("DataFlowIssue")
    void whenBuyerNumberValueNull_thenExpectError() {
        final ThrowingCallable result = () -> new BuyerId(null);

        assertThatThrownBy(result).isInstanceOf(NullPointerException.class);
    }

    @ParameterizedTest
    @ValueSource(longs = { -100, -25, -1, 0 })
    void whenBuyerNumberValueNegative_thenExpectError(final Long value) {
        final ThrowingCallable result = () -> new BuyerId(value);

        assertThatThrownBy(result).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void whenEmailValueIsNotNullAndHasValidFormat_thenExpectNoError() {
        final var input = "my-mail@may-provider.de";
        final Executable result = () -> new Email(input);

        assertDoesNotThrow(result);
        assertThat(Email.checkForFormatViolation(input)).isEmpty();
    }

    @Test
    @SuppressWarnings("DataFlowIssue")
    void whenEmailValueIsNull_thenExpectError() {
        final ThrowingCallable result = () -> new Email(null);

        assertThatThrownBy(result).isInstanceOf(NullPointerException.class);
    }

    @ParameterizedTest
    @ValueSource(strings = { "", " ", "test", "@my-provider.de", "my-mail@",
            "my-mail@my-provider" })
    void whenEmailHasNoValidFormat_thenExpectError(final String value) {
        final ThrowingCallable result = () -> new Email(value);

        assertThatThrownBy(result)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining(value);
        assertThat(Email.checkForFormatViolation(value)).isPresent();
    }

}
