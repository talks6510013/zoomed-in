package eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.application.port.out.SaveShoppingCartPort;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.BuyerId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.aggregate.ShoppingCart.ShoppingCartId;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.CreateShoppingCartCommand;
import eu.unite.fabianmittmann.talks.zoomed.in.hexagonal.service.shopping.domain.workflow.CreateShoppingCartWorkflow.ShoppingCartCreatedDomainEvent;

@SpringBootTest(classes = CreateShoppingCartService.class)
@ImportAutoConfiguration(ValidationAutoConfiguration.class)
@ActiveProfiles("test")
class CreateShoppingCartServiceIT {

    @Autowired
    CreateShoppingCartService uut;

    @MockBean
    CreateShoppingCartWorkflow createShoppingCartWorkflow;

    @MockBean
    SaveShoppingCartPort saveShoppingCartPort;

    @Test
    void createShoppingCart_whenValid_thenExpectSuccess() {
        // arrange
        final var command = new CreateShoppingCartCommand(123L);
        when(createShoppingCartWorkflow.run(any())).thenReturn(
                new ShoppingCartCreatedDomainEvent(new ShoppingCart(
                        new ShoppingCartId(UUID.randomUUID()), new BuyerId(123L))));

        // act
        final var result = uut.createShoppingCart(command);

        // assert
        assertThat(result.hasSuccess()).isTrue();
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(longs = { -123L, -1L, 0 })
    void createShoppingCart_whenCommandNotValid_thenExpectErrorAndNoShoppingCartSave(final Long value) {
        // arrange
        final var command = new CreateShoppingCartCommand(value);

        // act
        final var result = uut.createShoppingCart(command);

        // assert
        assertThat(result.hasSuccess()).isFalse();

        verifyNoInteractions(saveShoppingCartPort);
    }

}