# hexagonal-service

Sample project that represents the concepts
of [Hexagonal Architecture](https://alistair.cockburn.us/hexagonal-architecture/) in an
opinionated way.
